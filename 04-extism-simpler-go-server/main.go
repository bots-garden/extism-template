package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"

	"sync"

	"github.com/extism/extism"
)

// store all your plugins in a normal Go hash map, protected by a Mutex
var m sync.Mutex
var plugins = make(map[string]extism.Plugin)

func StorePlugin(plugin extism.Plugin) {
	// store all your plugins in a normal Go hash map, protected by a Mutex
	plugins["code"] = plugin
}

func GetPlugin() (extism.Plugin, error) {
	if plugin, ok := plugins["code"]; ok {
		return plugin, nil
	} else {
		return extism.Plugin{}, errors.New("🔴 no plugin")
	}
}

func main() {

	ctx := extism.NewContext()

	//extism.SetLogFile("./extism.log", "debug")

	defer ctx.Free() // this will free the context and all associated plugins

	manifest := extism.Manifest{Wasm: []extism.Wasm{extism.WasmFile{Path: "./hey-go/hey-go.wasm"}}}

	plugin, err := ctx.PluginFromManifest(manifest, []extism.Function{}, true)
	if err != nil {
		fmt.Println("🔴 When creating plugin", err)
		os.Exit(1)
	}
	//fmt.Println(plugin)

	StorePlugin(plugin)

	httpPort := "8080"

	http.HandleFunc("/", func(response http.ResponseWriter, request *http.Request) {

		if request.Method == "POST" {

			body, err := io.ReadAll(request.Body)

			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				io.WriteString(response, "😡 error with body: "+err.Error()+"\n")
				return
			}

			m.Lock()
			// don't forget to release the lock on the Mutex, sometimes its best to `defer m.Unlock()` right after yout get the lock
			defer m.Unlock()

			plugin, err := GetPlugin()

			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				io.WriteString(response, "😡 error when getting the plugin: "+err.Error()+"\n")
				return
			}
			// Call the wasm module
			result, err := plugin.Call("handler", body)
			if err != nil {
				response.WriteHeader(http.StatusInternalServerError)
				io.WriteString(response, "😡 error when calling the wasm module: "+err.Error()+"\n")
				return
			}

			// don't forget to release the lock on the Mutex, sometimes its best to `defer m.Unlock()` right after yout get the lock
			//m.Unlock()

			response.WriteHeader(http.StatusOK)
			io.WriteString(response, string(result)+"\n")

		} else {
			response.WriteHeader(http.StatusBadRequest)
			io.WriteString(response, "😡 this is not a post request\n")
		}

	})

	fmt.Println("🌍 http server is listening on:", httpPort)
	errHttpServer := http.ListenAndServe(":"+httpPort, nil)
	if errHttpServer != nil {
		fmt.Println("🔴 when creating http server", errHttpServer)
		os.Exit(1)
	}

}
