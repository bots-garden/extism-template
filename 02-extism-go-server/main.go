package main

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/extism/extism"
	"github.com/gofiber/fiber/v2"
)

var store sync.Map

func main() {
	
	ctx := extism.NewContext()

	//extism.SetLogFile("./extism.log", "debug")

    defer ctx.Free() // this will free the context and all associated plugins

	manifest := extism.Manifest{Wasm: []extism.Wasm{extism.WasmFile{Path: "./hey-go/hey-go.wasm"}}}

	store.Store("manifest", manifest)

	httpPort := "8080"
	app := fiber.New(fiber.Config{
		DisableStartupMessage: true,
		DisableKeepalive:      true,
		Concurrency:           100000,
	})

	app.Post("/", func(c *fiber.Ctx) error {
		
		params := c.Body()

		m, _ := store.Load("manifest")
		manifest := m.(extism.Manifest)

		plugin, err := ctx.PluginFromManifest(manifest, []extism.Function{}, true)
		if 	err!= nil {
			panic(err)
		}

		out, err := plugin.Call("handler", params)

	
		if err != nil {
			fmt.Println(err)
			c.Status(http.StatusConflict)
			return c.SendString(err.Error())
			//os.Exit(1)
		} else {
			c.Status(http.StatusOK)
			
			return c.SendString(string(out))
		}

	})

	fmt.Println("http server is listening on:", httpPort)
	app.Listen(":" + httpPort)
}
