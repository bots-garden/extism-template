import Fastify from 'fastify'
import process from "node:process"

import { Context, HostFunction, ValType } from '@extism/extism'
import { readFileSync } from 'fs'

let wasm = readFileSync(process.env.WASM_FILE)

const fastify = Fastify({
  logger: true
})

const opts = {}


// Host functions list
let functions = []

// Create the WASM plugin
let ctx = new Context()
let plugin = ctx.plugin(wasm, true, functions)
// Because of the event queue we can instantiate the plugin only one time
// https://www.geeksforgeeks.org/how-to-handle-concurrency-in-node-js/

// Create and start the HTTP server
const start = async () => {

  fastify.post('/', opts, async (request, reply) => {

    //let plugin = ctx.plugin(wasm, true, functions)

    // Call the WASM function, the request body is the argulent of the function
    let buf = await plugin.call("handler", request.body); 
    let result = buf.toString()

    return JSON.parse(result)
  })

  try {
    await fastify.listen({ port: process.env.HTTP_PORT, host: '0.0.0.0'})
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start().then(r => console.log("😄 started"))





