package main

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"time"
)



func GetUUID() string {
	return fmt.Sprintf("%x", md5.Sum([]byte(time.Now().String())))
}

func main() {
    fmt.Println("Hello, world!")
	fmt.Println(GetUUID())

	// Generate a simple HTTP server
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, world!")
	})

	// Start the server
	http.ListenAndServe(":8080", nil)
}

