package main

import (
	"capsule-http/capsule"
	"capsule-http/capsule/models"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"

	"github.com/extism/extism"
	"github.com/gofiber/fiber/v2"
)

// store all your plugins in a normal Go hash map, protected by a Mutex
var m sync.Mutex
var plugins = make(map[string]extism.Plugin)

func StorePlugin(plugin extism.Plugin) {
	// store all your plugins in a normal Go hash map, protected by a Mutex
	plugins["code"] = plugin
}

func GetPlugin() (extism.Plugin, error) {
	if plugin, ok := plugins["code"]; ok {
		return plugin, nil
	} else {
		return extism.Plugin{}, errors.New("🔴 no plugin")
	}
}

func main() {

	// --- BEGIN: Extism ---
	ctx := extism.NewContext()

	extism.SetLogFile("./extism.log", "debug")

	defer ctx.Free() // this will free the context and all associated plugins

	manifest := extism.Manifest{Wasm: []extism.Wasm{extism.WasmFile{Path: "../simple-go-plugin/hello-go.wasm"}}}
	//manifest := extism.Manifest{Wasm: []extism.Wasm{extism.WasmFile{Path: "./hello-go.wasm"}}}

	plugin, err := ctx.PluginFromManifest(manifest, []extism.Function{}, true)
	if err != nil {
		log.Println("🔴 !!! Error when loading the plugin", err)
		os.Exit(1)
	}

	StorePlugin(plugin)
	// --- END: Extism ---

	httpPort := "8080"
	app := fiber.New(fiber.Config{
		DisableStartupMessage: true,
		DisableKeepalive:      true,
		Concurrency:           100000,
	})

	/*
	   curl -v -X POST \
	   http://localhost:8080 \
	   -H 'content-type: text/plain; charset=utf-8' \
	   -d 'Bob Morane'
	   echo ""
	*/

	app.Post("/", func(c *fiber.Ctx) error {

		//params := c.Body()
		//fmt.Println(string(params))

		// build headers JSON string
		var headers []string
		for field, value := range c.GetReqHeaders() {
			headers = append(headers, `"`+field+`":"`+value+`"`)
		}
		headersStr := strings.Join(headers[:], ",")

		requestParam := models.Request{
			Body: string(c.Body()),
			//JSONBody: string(c.Body()), //! to use in the future
			//TextBody: string(c.Body()), //! to use in the future
			URI:     c.Request().URI().String(),
			Method:  c.Method(),
			Headers: headersStr,
		}

		//fmt.Println("👋 requestParam:", requestParam)

		JSONData, err := json.Marshal(requestParam)

		if err != nil {
			log.Println("❌ Error when reading the request parameter", err)
			c.Status(http.StatusInternalServerError) // .🤔
			return c.SendString(err.Error())
		}

		m.Lock()
		// don't forget to release the lock on the Mutex, sometimes its best to `defer m.Unlock()` right after yout get the lock
		defer m.Unlock()
		
		plugin, err := GetPlugin()

		if err != nil {
			log.Println("🔴 !!! Error when loading the plugin", err)
			c.Status(http.StatusInternalServerError) //.🤔", err)
			//os.Exit(1)
			return c.SendString(err.Error())
		}

		/*
			_, errStart := plugin.Call("_start", nil)
			if errStart!= nil {
				log.Println("🔴!!! Error when starting the plugin", errStart)
				c.Status(http.StatusInternalServerError) //.🤔", err)
				os.Exit(1)
			}
		*/

		responseBuffer, err := plugin.Call("initialize", JSONData)
		if err != nil {
			log.Println("❌ Error when calling initialize", err)
			c.Status(http.StatusInternalServerError) // .🤔 c.Status(http.StatusConflict)
			return c.SendString(err.Error())
		}

		responseFromWasmGuest, err := capsule.Result(responseBuffer)
		if err != nil {
			log.Println("❌ Error when getting the Result", err)
			c.Status(http.StatusInternalServerError) // .🤔
			return c.SendString(err.Error())
		}

		// unmarshal the response
		var response models.Response

		//! if TextBody contains "\n" or quotes there is an error (fix something in capsule-module-sdk/hande.http.go => callHandleHTTP)
		errMarshal := json.Unmarshal(responseFromWasmGuest, &response)
		if errMarshal != nil {
			log.Println("❌ Error when unmarshal the response", errMarshal)
			c.Status(http.StatusInternalServerError) // .🤔
			return c.SendString(errMarshal.Error())
		}

		c.Status(response.StatusCode)

		// set headers
		for key, value := range response.Headers {
			c.Set(key, value)
		}

		if len(response.TextBody) > 0 {
			decodedStrAsByteSlice, _ := base64.StdEncoding.DecodeString(string(response.TextBody))

			// send text body
			//return c.SendString(response.TextBody)
			return c.SendString(string(decodedStrAsByteSlice))
		}
		// send JSON body
		jsonStr, err := json.Marshal(response.JSONBody)
		if err != nil {
			log.Println("❌ Error when marshal the body", err)
			c.Status(http.StatusInternalServerError) // .🤔
			return c.SendString(errMarshal.Error())
		}

		return c.Send(jsonStr)

	})

	fmt.Println("http server is listening on:", httpPort)
	app.Listen(":" + httpPort)
}
