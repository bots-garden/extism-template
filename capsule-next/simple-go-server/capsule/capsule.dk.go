// Package capsule SDK for host applications
package capsule

import (
	"errors"
)

const isFailure = rune('F')
const isSuccess = rune('S')

// success appends the isSuccess byte to the beginning of the input buffer and returns the result.
//
// buffer: byte slice to append isSuccess byte to.
// []byte: byte slice with the appended isSuccess byte.
func success(buffer []byte) []byte {
	return append([]byte(string(isSuccess)), buffer...)
}

// failure appends a string "isFailure" to the given byte slice buffer and returns the new slice.
//
// buffer: the byte slice to which "isFailure" is appended.
// Returns the new byte slice with the string "isFailure" appended to it.
func failure(buffer []byte) []byte {
	return append([]byte(string(isFailure)), buffer...)
}

// Result returns the data without the first byte if the first byte is isSuccess.
// Otherwise, it returns nil and an error with the data starting from the second byte.
//
// data: A byte slice containing the data to check.
// []byte: The data without the first byte if the first byte is isSuccess.
// error: If the first byte is not isSuccess, it returns an error with the data starting from the second byte.
func Result(data []byte,) ([]byte, error) {
	if data[0] == byte(isSuccess) {
		return data[1:], nil
	}
	return nil, errors.New(string(data[1:]))
}
