// package main
package main

import (
	"simple/capsule"
)


//export initialize
func initialize() {

	capsule.SetHandleHTTP(func (param capsule.HTTPRequest) (capsule.HTTPResponse, error) {

		return capsule.HTTPResponse{
			TextBody: "🍊 👋 Hey " + param.Body +" !",
			Headers: `{"Content-Type": "text/plain; charset=utf-8"}`,
			StatusCode: 200,
		}, nil
		
	})

}

func main() {}
