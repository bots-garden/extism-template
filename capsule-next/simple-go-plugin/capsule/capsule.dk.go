package capsule

import (
	"errors"
	"github.com/extism/go-pdk"
)

const isFailure = rune('F')
const isSuccess = rune('S')

func success(buffer []byte) int32 {
	// allocate the memory
	mem := pdk.AllocateBytes(append([]byte(string(isSuccess)), buffer...))
	// copy output to host memory
	pdk.OutputMemory(mem)

	return 0 // useless
}

func failure(buffer []byte) int32 {
	// allocate the memory
	mem := pdk.AllocateBytes(append([]byte(string(isFailure)), buffer...))
	// copy output to host memory
	pdk.OutputMemory(mem)

	return 0 // useless
}

// Success function
func Success(buffer []byte) int32 {
	return success(buffer)
}

// Failure function
func Failure(buffer []byte) int32 {
	return failure(buffer)
}

// Result function
func Result(data []byte,) ([]byte, error) {
	if data[0] == byte(isSuccess) {
		return data[1:], nil
	}
	return nil, errors.New(string(data[1:]))
}