// package capsule
package capsule

import (
	"encoding/base64"
	"strconv"

	"github.com/extism/go-pdk"
	"github.com/valyala/fastjson"
)

func SetHandleHTTP(function func(param HTTPRequest) (HTTPResponse, error)) {
	request, err := GetHTTPRequest()
	if err != nil {
		Failure([]byte(err.Error()))
	}

	response, err := function(request)
	if err != nil {
		Failure([]byte(err.Error()))
	}

	Success([]byte(SetHTTPResponse(response)))
}

// HTTPRequest is the data of the http request
type HTTPRequest struct {
	Body     string
	JSONBody string
	TextBody string
	URI      string
	Method   string
	Headers  string
}

// HTTPResponse is the data of the http response
type HTTPResponse struct {
	//Body    string
	JSONBody   string
	TextBody   string
	Headers    string
	StatusCode int
}


func GetHTTPRequest() (HTTPRequest, error) {
	// read function argument from the memory
	// The data comes from the host application
	JSONDataBuffer := pdk.Input()

	parser := fastjson.Parser{}
	JSONData, err := parser.ParseBytes(JSONDataBuffer)
	if err!= nil {
		return HTTPRequest{}, err
	}

	httpRequestParam := HTTPRequest{
		Body: string(JSONData.GetStringBytes("Body")),
		//JSONBody: string(JSONData.GetStringBytes("JSONBody")), //! to use in the future
		//TextBody: string(JSONData.GetStringBytes("TextBody")), //! to use in the future
		URI:     string(JSONData.GetStringBytes("URI")),
		Method:  string(JSONData.GetStringBytes("Method")),
		Headers: string(JSONData.GetStringBytes("Headers")),
	}

	return httpRequestParam, nil
}

func SetHTTPResponse(resp HTTPResponse) string {

	var jsonBody string
	if len(resp.JSONBody) == 0 {
		jsonBody = "{}"
	} else {
		jsonBody = resp.JSONBody
	}

	var textBody string
	if len(resp.TextBody) == 0 {
		textBody = ""
	} else {
		// avoid special characters in jsonString
		textBody = base64.StdEncoding.EncodeToString([]byte(resp.TextBody))
		//textBody = retValue.TextBody
	}

	jsonHTTPResponse := `{"JSONBody":` + jsonBody + `,"TextBody":"` + textBody + `","Headers":` + resp.Headers + `,"StatusCode":` + strconv.Itoa(resp.StatusCode) + `}`
	return jsonHTTPResponse
}