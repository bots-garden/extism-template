package main

import (
	"fmt"
	"os"

	"github.com/extism/extism"
)

func main() {
	
	// --- BEGIN: Extism ---
	ctx := extism.NewContext()
	
    defer ctx.Free() // this will free the context and all associated plugins

	manifest := extism.Manifest{Wasm: []extism.Wasm{extism.WasmFile{Path: "./hello-go/hello-go.wasm"}}}
    
	manifest.AllowedHosts = []string{"*"}
	manifest.AllowedPaths = map[string]string{"./hello-go": "./hello-go"}
	
	manifest.Config = map[string]string{"route": "https://jsonplaceholder.typicode.com/todos/1"}

	extism.SetLogFile("./hello-go/hello-go.log", "0")



	plugin, err := ctx.PluginFromManifest(manifest, []extism.Function{}, true)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	
	
	out, err := plugin.Call("say_hello", []byte("BOB"))

	if err != nil {
		fmt.Println(err)
		//os.Exit(1)
	} else {
		fmt.Println(string(out))
	}



}
