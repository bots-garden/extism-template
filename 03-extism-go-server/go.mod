module launcher-http-bis

go 1.30

require (
	github.com/extism/extism v0.4.0
	github.com/gofiber/fiber/v2 v2.46.0
)

require (
	github.com/valyala/fastjson v1.6.4 // indirect
	golang.org/x/mod v0.7.0 // indirect
	golang.org/x/tools v0.4.0 // indirect
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/extism/go-pdk v0.0.0-20230119214914-65bffbeb3e64
	github.com/google/uuid v1.3.0 // indirect
	github.com/klauspost/compress v1.16.6 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/philhofer/fwd v1.1.2 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/savsgio/dictpool v0.0.0-20221023140959-7bf2e61cea94 // indirect
	github.com/savsgio/gotils v0.0.0-20230208104028-c358bd845dee // indirect
	github.com/tinylib/msgp v1.1.8 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.47.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
)
