package main

import (
	"github.com/extism/go-pdk"
)

//export handler
func handler() int32 {
	
	// Read the memory
	input := pdk.Input()

	output := `{"message":"👋 Hello World 🌍 from Go","input": "` + string(input) + `"}`

	//pdk.Log(pdk.LogDebug, "BOB MORANE")

	mem := pdk.AllocateString(output)
	
	// copy the data to the memory
	pdk.OutputMemory(mem)

	return 0
}

func main() {}
