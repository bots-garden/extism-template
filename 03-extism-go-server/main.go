package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	"sync"

	"github.com/extism/extism"
	"github.com/gofiber/fiber/v2"
)

var store sync.Map

func StorePlugin(plugin extism.Plugin) {
	store.Store("plugin", plugin)
}

func GetPlugin() (extism.Plugin, error) {
	value, ok := store.Load("plugin")
	if !ok {
		return extism.Plugin{}, errors.New("ouch")
	}
	return value.(extism.Plugin), nil
}


func main() {

	ctx := extism.NewContext()

	//extism.SetLogFile("./extism.log", "debug")

    defer ctx.Free() // this will free the context and all associated plugins

	manifest := extism.Manifest{Wasm: []extism.Wasm{extism.WasmFile{Path: "./hey-go/hey-go.wasm"}}}
	
	plugin, err := ctx.PluginFromManifest(manifest, []extism.Function{}, true)
	if err != nil {
		fmt.Println("🔴 When creating plugin", err)
		os.Exit(1)
	}
	//fmt.Println(plugin)
	
	StorePlugin(plugin)
	

	httpPort := "8080"
	app := fiber.New(fiber.Config{
		DisableStartupMessage: true,
		DisableKeepalive:      true,
		Concurrency:           100000, // 100000

	})
	

	/*
	fake := func(data []byte) ([]byte, error) {
		return data, nil
	}
	fmt.Println(fake([]byte("")))
	*/

	app.Post("/", func(c *fiber.Ctx) error {
		
		params := c.Body()

		
		plugin, err := GetPlugin()
		if err != nil {
			fmt.Println("😡", err.Error())
		}
		
		
		out, err := plugin.Call("handler", params)
		
		if err!= nil {
			fmt.Println("😡", err.Error())
		}
		
		//out, err := fake(params)

		if err != nil {
			fmt.Println(err)
			c.Status(http.StatusConflict)
			return c.SendString(err.Error())
		} else {
			c.Status(http.StatusOK)
			return c.SendString(string(out))
		}

	})

	fmt.Println("http server is listening on:", httpPort)
	app.Listen(":" + httpPort)
}
